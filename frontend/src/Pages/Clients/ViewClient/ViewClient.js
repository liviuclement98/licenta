import { Button } from '@material-ui/core';
import React, { useState, useEffect } from 'react';

import { useParams, useHistory } from 'react-router-dom';

import Backdrop from '../../../Components/Shared/Backdrop/Backdrop';
import EditClientModal from '../../../Components/ViewClient/Modals/EditClientModal';
import PageHeader from '../../../Components/Shared/PageHeader/PageHeader';
import Dialog from '../../../Components/Shared/Dialog/Dialog';
import AppointmentHistoryTable from '../../../Components/ViewClient/Tables/AppointmentHistoryTable';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import axios from 'axios';

import './ViewClient.css';

const getConvertedDate = (date) => {
  date = new Date(date);

  let dd = String(date.getDate()).padStart(2, '0');
  let mm = String(date.getMonth() + 1).padStart(2, '0');
  let yyyy = date.getFullYear();

  return dd + '-' + mm + '-' + yyyy;
};

const ViewClients = (props) => {
  let { id } = useParams();

  let history = useHistory();

  const clientData = {};

  const [company, setCompany] = useState('Aeroclubul Romaniei');
  const [name, setName] = useState(clientData.name);
  const [phone, setPhone] = useState(clientData.phone);
  const [email, setEmail] = useState(clientData.email);
  const [address, setAddress] = useState(clientData.address);
  const [activeTab, setActiveTab] = useState(0);
  const [showDialog, setShowDialog] = useState(false);
  const [showEditClientModal, setShowEditClientModal] = useState(false);
  const [appointments, setAppointments] = useState([]);
  const [loading, setLoading] = useState(true);

  const COLUMNS = [
    { id: 'id', label: 'ID', minWidth: 100 },
    { id: 'date', label: 'Date', minWidth: 100, type: Date },
    { id: 'description', label: 'Description', minWidth: 100 },
    { id: 'type', label: 'Type', minWidth: 100 },
    { id: 'workers', label: 'Workers', minWidth: 100 },
    { id: 'price', label: 'Price', minWidth: 100 },
    { id: 'observations', label: 'Observations', minWidth: 100 },
  ];

  useEffect(() => {
    setLoading(true);
    axios
      .get(process.env.REACT_APP_BACKEND_URL + `/clients/${id}`)
      .then((res) => {
        setName(res.data.client.name);
        setPhone(res.data.client.phone);
        setEmail(res.data.client.email);
        setAddress(res.data.client.address);
        setCompany(res.data.client.company);
        let parsedAppointments = [];
        res.data.appointments.forEach((appointment) => {
          appointment.datesEmployees.forEach((dateEmployees) => {
            let employees = dateEmployees.employeesTimes.map(
              (employeeTime) => employeeTime.name
            );
            employees = employees.join(', ');
            parsedAppointments.push({
              id: appointment._id,
              date: getConvertedDate(dateEmployees.date),
              description: appointment.description,
              type: 'instalatii',
              workers: employees,
              price: '100',
              observations: appointment.observations || '',
            });
          });
        });
        setAppointments(parsedAppointments);
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
      });
  }, []);

  const tabChangeHandler = (tab) => {
    setActiveTab(tab);
  };

  const deleteClientHandler = () => {
    setShowDialog(false);
    axios
      .delete(process.env.REACT_APP_BACKEND_URL + `/clients/${id}`)
      .then((res) => {
        console.log(res);
        history.push('/clients');
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const updateClientHandler = (company, name, phone, email, address) => {
    axios
      .patch(process.env.REACT_APP_BACKEND_URL + `/clients/${id}`, {
        company,
        name,
        phone,
        email,
        address,
      })
      .then((res) => {
        setCompany(company);
        setName(name);
        setPhone(phone);
        setEmail(email);
        setAddress(address);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <>
      {showEditClientModal && (
        <EditClientModal
          onClose={() => setShowEditClientModal(false)}
          onUpdate={updateClientHandler}
          data={{ company, name, phone, email, address }}
        />
      )}
      {showEditClientModal && (
        <Backdrop onClick={() => setShowEditClientModal(false)} />
      )}
      <Dialog
        show={showDialog}
        onConfirm={deleteClientHandler}
        onClose={() => setShowDialog(false)}
        title='Delete Client?'
        text={
          appointments.length > 0
            ? 'Deleting the client will also delete his appointments. Proceed?'
            : 'This action is final. Proceed?'
        }
        confirm='yes'
        cancel='no'
      />
      <div className='view-client'>
        <PageHeader title='View Client'>
          <Button
            variant='contained'
            color='primary'
            style={{ marginRight: 10 }}
            onClick={() => setShowEditClientModal(true)}
          >
            <EditIcon
              style={{ fontSize: '1rem', marginRight: 3, color: 'white' }}
            />
            Edit
          </Button>
          <Button
            variant='contained'
            color='secondary'
            onClick={() => setShowDialog(true)}
          >
            <DeleteIcon
              style={{ fontSize: '1rem', marginRight: 3, color: 'white' }}
            />
            Delete
          </Button>
        </PageHeader>
        <div className='client-card'>
          <div className='info-line'>
            <p>Company:</p>
            <span>{company}</span>
          </div>
          <div className='info-line'>
            <p>Name:</p>
            <span>{name}</span>
          </div>
          <div className='info-line'>
            <p>Address:</p>
            <span>{address}</span>
          </div>
          <div className='info-line'>
            <p>Phone:</p>
            <span>{phone}</span>
          </div>
          <div className='info-line'>
            <p>Email:</p>
            <span>{email}</span>
          </div>
        </div>
        <div className='tabs'>
          <div
            className={`tab ${activeTab === 0 ? 'active' : ''}`}
            id='app_history'
            value={0}
            onClick={() => tabChangeHandler(0)}
          >
            Appointment History
          </div>
          <div
            className={`tab ${activeTab === 1 ? 'active' : ''}`}
            id='subscription'
            value={1}
            onClick={() => tabChangeHandler(1)}
          >
            Subscription
          </div>
        </div>
        <div className='tabs-content'>
          <div className={`content ${activeTab === 0 ? 'active' : ''}`}>
            {!loading &&
              (appointments.length === 0 ? (
                <h3 className='title'>This client has no appointments yet.</h3>
              ) : (
                <AppointmentHistoryTable
                  rows={appointments}
                  headCells={COLUMNS}
                  isLoading={loading}
                />
              ))}
          </div>
          <div className={`content ${activeTab === 1 ? 'active' : ''}`}>
            <h3 className='title'>This client has no subscriptions.</h3>
          </div>
        </div>
      </div>
    </>
  );
};

export default ViewClients;
