import React, { useEffect, useState } from 'react';
import './Clients.css';

import ClientsTable from '../../Components/Clients/Tables/ClientsTable';
import PageHeader from '../../Components/Shared/PageHeader/PageHeader';
import Backdrop from '../../Components/Shared/Backdrop/Backdrop';

import { Button } from '@material-ui/core';
import AddClientModal from '../../Components/Clients/Modals/AddClientModal';
import axios from 'axios';

const Clients = () => {
  const [showAddClientModal, setShowAddClientModal] = useState(false);
  const [rows, setRows] = useState([]);
  const [loading, setLoading] = useState(true);
  const columns = [
    { id: 'id', label: 'ID', minWidth: 170 },
    { id: 'company', label: 'Company', minWidth: 170 },
    { id: 'name', label: 'Name', minWidth: 170 },
    { id: 'email', label: 'Email', minWidth: 100 },
    {
      id: 'phone',
      label: 'Phone',
      minWidth: 170,
      align: 'right',
      format: (value) => value.toLocaleString('en-US'),
    },
    {
      id: 'address',
      label: 'Address',
      minWidth: 170,
      align: 'right',
      format: (value) => value.toLocaleString('en-US'),
    },
  ];

  console.log('im rendering');

  useEffect(() => {
    setLoading(true);
    axios
      .get(process.env.REACT_APP_BACKEND_URL + '/clients')
      .then((response) => {
        console.log(response.data.clients);
        setRows(
          response.data.clients.map((client) => ({
            id: client.id,
            company: client.company,
            name: client.name,
            email: client.email,
            phone: client.phone,
            address: client.address,
          }))
        );
        console.log(rows);
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
      });
  }, []);

  const closeModal = () => {
    setShowAddClientModal(false);
  };

  const showModal = () => {
    setShowAddClientModal(true);
  };

  const addClientHandler = async (company, name, phone, email, address) => {
    axios
      .post(process.env.REACT_APP_BACKEND_URL + '/clients', {
        company,
        name,
        email,
        phone,
        address,
      })
      .then((res) => {
        console.log('res: ', res);
        const updatedRows = [...rows];
        updatedRows.push({
          id: res.data.client._id,
          company,
          name,
          email,
          phone,
          address,
        });
        console.log(updatedRows);
        setRows(updatedRows);
      })
      .catch((err) => console.log(err));
  };

  return (
    <>
      {showAddClientModal && <Backdrop onClick={closeModal} />}
      {showAddClientModal && (
        <AddClientModal onClose={closeModal} onAdd={addClientHandler} />
      )}
      <div className='clients'>
        <PageHeader title='Clients'>
          <Button variant='contained' color='primary' onClick={showModal}>
            Add
          </Button>
        </PageHeader>
        {!loading &&
          (rows.length === 0 ? (
            <h3 className='title'>No clients found!</h3>
          ) : (
            <ClientsTable rows={rows} headCells={columns} />
          ))}
      </div>
    </>
  );
};

export default Clients;
