import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';

import './Login.css';

import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import { useDispatch } from 'react-redux';
import { login } from '../../../Redux/Actions/index';
import axios from 'axios';

const checkemail = (input) => {
  return input.length < 6;
};

const checkPassword = (input) => {
  return input.length < 6;
};

const useStyles = makeStyles((theme) => ({
  margin: {
    marginBottom: theme.spacing(3),
  },
}));

const Login = (props) => {
  const [email, setEmail] = useState({
    value: '',
    error: true,
    isTouched: false,
  });
  const [password, setPassword] = useState({
    value: '',
    error: true,
    isTouched: false,
  });

  const [isLoading, setIsLoading] = useState(false);

  const history = useHistory();

  const classes = useStyles();
  const dispatch = useDispatch();

  const emailChangeHandler = (event) => {
    setEmail({
      ...email,
      value: event.target.value,
      error: checkemail(event.target.value),
    });
  };

  const passwordChangeHandler = (event) => {
    setPassword({
      ...password,
      value: event.target.value,
      error: checkPassword(event.target.value),
    });
  };

  const setTouched = () => {
    setEmail({ ...email, isTouched: true });
    setPassword({ ...password, isTouched: true });
  };

  const formSubmitHandler = (event) => {
    event.preventDefault();
    setTouched();

    console.log('envv: ', process.env.REACT_APP_BACKEND_URL);

    if (!email.error && !password.error) {
      setIsLoading(true);
      const userData = { email: email.value, password: password.value };

      axios
        .post(process.env.REACT_APP_BACKEND_URL + '/users/login', userData)
        .then((response) => {
          // const token = response.data.token;
          setIsLoading(false);

          dispatch(login());
        })
        .catch((err) => {
          alert('Invalid login credentials');
          setIsLoading(false);

          console.log(err);
        });
    }
  };

  return (
    <div className='login'>
      {isLoading && <CircularProgress />}
      <h1 className='title'>Login</h1>
      <form className='login_form' onSubmit={formSubmitHandler}>
        <TextField
          className={classes.margin}
          error={email.error && email.isTouched}
          helperText={
            email.error && email.isTouched ? 'Enter at least 6 characters' : ''
          }
          label='email'
          variant='outlined'
          onChange={emailChangeHandler}
          onBlur={() => setEmail({ ...email, isTouched: true })}
        />
        <TextField
          className={classes.margin}
          error={password.error && password.isTouched}
          helperText={
            password.error && password.isTouched
              ? 'Enter at least 6 characters'
              : ''
          }
          label='Password'
          variant='outlined'
          type='password'
          onChange={passwordChangeHandler}
          onBlur={() => setPassword({ ...password, isTouched: true })}
        />
        <Button
          size='large'
          variant='contained'
          color='primary'
          type='submit'
          className={classes.margin}
        >
          Login
        </Button>
      </form>
    </div>
  );
};

export default Login;
