import React, { useState, useEffect, useRef } from 'react';
import ReactDOM from 'react-dom';
import './Schedule.css';
import ScheduleTable from '../../Components/Schedule/Tables/ScheduleTable';
import Backdrop from '../../Components/Shared/Backdrop/Backdrop';
import AddAppointmentModal from '../../Components/Schedule/Modals/AddAppointmentModal';
import ViewAppointmentModal from '../../Components/Schedule/Modals/ViewAppointmentModal';
import PageHeader from '../../Components/Shared/PageHeader/PageHeader';
import { TextField } from '@material-ui/core';

import { getConvertedDate } from '../../Utils/Date';

import axios from 'axios';

const getDate = (dayDifNr = 0) => {
  let today = new Date();

  if (dayDifNr) {
    today.setDate(new Date().getDate() + dayDifNr);
  }

  let dd = String(today.getDate()).padStart(2, '0');
  let mm = String(today.getMonth() + 1).padStart(2, '0');
  let yyyy = today.getFullYear();

  return (today = yyyy + '-' + mm + '-' + dd);
};

const getAppointmentId = (datesEmployees) => {
  let employeeIds = '';
  let dates = '';
  let startTimes = '';
  let sufix = '';

  const datesLen = datesEmployees.length;
  for (let i in datesEmployees) {
    sufix =
      i < datesLen - 1 ? datesEmployees[i].date + '_' : datesEmployees[i].date;
    dates += sufix;
    const employeesTimesLen = datesEmployees[i].employeesTimes.length;
    for (let j in datesEmployees[i].employeesTimes) {
      sufix =
        j < employeesTimesLen - 1
          ? datesEmployees[i].employeesTimes[j].id + '_'
          : datesEmployees[i].employeesTimes[j].id;
      employeeIds += sufix;
      sufix =
        j < employeesTimesLen - 1
          ? datesEmployees[i].employeesTimes[j].startTime + '_'
          : datesEmployees[i].employeesTimes[j].startTime;
      startTimes += sufix;
    }
  }

  return `${dates}>${employeeIds}>${startTimes}`;
};

const _MS_PER_DAY = 1000 * 60 * 60 * 24;

function dateDiffInDays(a, b) {
  const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

Date.prototype.addDays = function (days) {
  let date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
};

function getDates(startDate, stopDate) {
  let dateArray = [];
  let currentDate = startDate;
  while (currentDate <= stopDate) {
    let day = new Date(currentDate).getDay();
    if (day !== 0 && day !== 6) {
      dateArray.push(getConvertedDate(currentDate));
    } else {
      dateArray.push('weekend');
    }
    currentDate = currentDate.addDays(1);
  }
  return dateArray;
}

const updateCanvas = (
  appointments,
  setAppointmentToView,
  setShowViewAppointmentModal,
  color
) => {
  console.log('updating the canvas');
  console.log(appointments);
  appointments.forEach((appointment) => {
    const { datesEmployees, description } = appointment;

    datesEmployees.forEach((dateEmployee) => {
      // const formattedDate = dateEmployee.date;

      dateEmployee.employeesTimes.forEach((employeeTimes) => {
        employeeTimes.duration = parseFloat(employeeTimes.duration) || 4;
        const width = employeeTimes.duration * 200.14;

        const overlayDiv = (
          <div
            className={appointment.canvasId}
            style={{
              color: '#fff',
              backgroundColor: color, // '#FA8072', //#3a243b #36013f
              width: `${width}%`,
              position: 'absolute',
              top: 0,
              bottom: 0,
              left: 0,
              zIndex: 6,
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              padding: '0 1rem',
              overflow: 'hidden',
            }}
            onClick={(event) => {
              event.stopPropagation();
              setAppointmentToView({
                ...appointments.find((appointment) => {
                  console.log('app canvas id:', appointment.canvasId);
                  console.log('classname: ', event.target.className);
                  return appointment.canvasId === event.target.className;
                }),
              });

              setShowViewAppointmentModal(true);
            }}
          >
            {description}
          </div>
        );

        const cellId = `${dateEmployee.date}_${employeeTimes.id}_${employeeTimes.startTime}`;
        const cell = document.getElementById(cellId);
        if (cell) {
          ReactDOM.render(overlayDiv, cell);
        }
      });
    });
  });
};

const Schedule = (props) => {
  const [showAddAppointmentModal, setShowAddAppointmentModal] = useState(false);
  const [showViewAppointmentModal, setShowViewAppointmentModal] =
    useState(false);
  const [startTime, setStartTime] = useState();
  const [selectedEmployee, setSelectedEmployee] = useState();
  const [date, setDate] = useState(getDate());
  const [startDate, setStartDate] = useState(getDate());
  const [endDate, setEndDate] = useState(getDate(6));
  const [employees, setEmployees] = useState([]);
  const [loading, setLoading] = useState(true);

  const [appointments, setAppointments] = useState([]);
  const [appointmentToView, setAppointmentToView] = useState();

  const [color, setColor] = useState('');

  const renderCycle = useRef(0);

  useEffect(() => {
    (async () => {
      try {
        const result = await axios.get(
          process.env.REACT_APP_BACKEND_URL +
            `/schedule?from=${startDate}&to=${endDate}`
        );
        setLoading(false);
        setEmployees(result.data.workers);
        setColor(result.data.calendarColor);
        setAppointments(
          result.data.appointments.map((appointment) => ({
            ...appointment,
            name: appointment.client[0].name,
            company: appointment.client[0].company,
            address: appointment.client[0].address,
            phone: appointment.client[0].phone,
            email: appointment.client[0].email,
            datesEmployees: appointment.datesEmployees.map((dateEmployee) => ({
              ...dateEmployee,
              date: getConvertedDate(dateEmployee.date),
            })),
          }))
        );
        console.log(result.data);
      } catch (err) {
        setLoading(false);
        console.log(err);
      }
    })();
  }, [startDate, endDate]);

  const a = new Date(startDate);
  const b = new Date(endDate);

  const dates = getDates(a, b);

  useEffect(() => {
    document.body.style.overflow =
      showAddAppointmentModal || showViewAppointmentModal ? 'hidden' : 'unset';
  }, [showAddAppointmentModal, showViewAppointmentModal]);

  useEffect(() => {
    updateCanvas(
      appointments,
      setAppointmentToView,
      setShowViewAppointmentModal,
      color
    );
  }, [appointments, startDate, endDate]);

  console.log('appointments: ', appointments);

  //handle cell click
  const cellClickHandler = (employeeId, startTime, eventTarget, date) => {
    setSelectedEmployee(employeeId);
    setStartTime(startTime);
    setDate(date);
    setShowAddAppointmentModal(true);
  };

  const closeModal = () => {
    console.table('appointmentToView', appointmentToView);
    setShowAddAppointmentModal(false);
    setShowViewAppointmentModal(false);
  };

  const deleteAppointmentHandler = () => {
    axios
      .delete(process.env.REACT_APP_BACKEND_URL + '/schedule', {
        data: { _id: appointmentToView._id },
      })
      .then((res) => {
        console.log(res);
        setAppointments((prevAppointments) => {
          let updatedAppointments = [...prevAppointments];
          updatedAppointments = updatedAppointments.filter(
            (app) => app.canvasId !== appointmentToView.canvasId
          );
          return updatedAppointments;
        });

        appointmentToView.datesEmployees.forEach((dateEmployees) => {
          // const dateToDelete = getConvertedDate(dateEmployees.date);

          dateEmployees.employeesTimes.forEach((employee) => {
            let root = document.getElementById(
              `${dateEmployees.date}_${employee.id}_${employee.startTime}`
            );
            if (root) {
              ReactDOM.unmountComponentAtNode(root);
            }
          });
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const saveAppointmentHandler = (appObj) => {
    const newAppointment = {
      canvasId: getAppointmentId(appObj.datesEmployees),
      datesEmployees: appObj.datesEmployees,
      description: appObj.description,
      company: appObj.companyName,
      name: appObj.contactName,
      phone: appObj.phoneNr,
      address: appObj.address,
      email: appObj.email,
      type: appObj.type,
      status: appObj.status,
      comunicatedPrice: appObj.comunicatedPrice,
      chargedPrice: appObj.chargedPrice,
      observations: appObj.observations,
    };
    console.log('newAppointment: ', newAppointment);
    axios
      .post(process.env.REACT_APP_BACKEND_URL + '/schedule', newAppointment)
      .then((res) => {
        console.log('res', res);
        const updatedAppointments = [...appointments];
        updatedAppointments.push({
          ...newAppointment,
          _id: res.data.newAppointment._id,
        });
        setAppointments(updatedAppointments);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const updateAppointmentHandler = (appObj) => {
    console.log('newDatesEmployees: ', appObj.newDatesEmployees);
    axios
      .patch(process.env.REACT_APP_BACKEND_URL + '/schedule/', {
        _id: appointmentToView._id,
        newDatesEmployees: appObj.datesEmployees,
        newDescription: appObj.description,
        newType: appObj.type,
        newStatus: appObj.status,
        newComunicatedPrice: appObj.comunicatedPrice,
        newChargedPrice: appObj.chargedPrice,
        newObservations: appObj.observations,
      })
      .then((res) => {
        const index = appointments
          .map((app) => app.canvasId)
          .indexOf(appointmentToView.canvasId);

        appointmentToView.datesEmployees.forEach((dateEmployees) => {
          const dateToDelete = dateEmployees.date;

          dateEmployees.employeesTimes.forEach((employeeTimes) => {
            const root = document.getElementById(
              `${dateToDelete}_${employeeTimes.id}_${employeeTimes.startTime}`
            );
            if (root) {
              ReactDOM.unmountComponentAtNode(root);
            }
          });
        });

        const updatedAppointments = [...appointments];

        updatedAppointments[index] = {
          _id: appointmentToView._id,
          name: appointmentToView.name,
          company: appointmentToView.company,
          address: appointmentToView.address,
          email: appointmentToView.email,
          phone: appointmentToView.phone,
          canvasId: getAppointmentId(appObj.datesEmployees),
          datesEmployees: appObj.datesEmployees,
          description: appObj.description,
          type: appObj.type,
          status: appObj.status,
          comunicatedPrice: appObj.comunicatedPrice,
          chargedPrice: appObj.chargedPrice,
          observations: appObj.observations,
        };

        setAppointments(updatedAppointments);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <>
      {(showAddAppointmentModal || showViewAppointmentModal) && (
        <Backdrop onClick={closeModal} />
      )}
      {showAddAppointmentModal && (
        <AddAppointmentModal
          onClose={closeModal}
          employees={employees}
          selectedEmployee={selectedEmployee}
          startTime={startTime}
          date={date}
          onAdd={saveAppointmentHandler}
        />
      )}
      {showViewAppointmentModal && (
        <ViewAppointmentModal
          onClose={closeModal}
          employees={employees}
          appointment={appointmentToView}
          onUpdate={updateAppointmentHandler}
          onDelete={deleteAppointmentHandler}
        />
      )}
      <div className='schedule'>
        <PageHeader title='Schedule'>
          <div className='from-date'>
            <TextField
              id='date'
              label='From'
              variant='outlined'
              type='date'
              defaultValue={startDate}
              onChange={(event) => setStartDate(event.target.value)}
              InputLabelProps={{
                shrink: true,
              }}
              style={{ marginRight: 20 }}
            />
          </div>
          <div className='to-date'>
            <TextField
              id='date'
              label='To'
              variant='outlined'
              type='date'
              defaultValue={endDate}
              onChange={(event) => setEndDate(event.target.value)}
              InputLabelProps={{
                shrink: true,
              }}
            />
          </div>
        </PageHeader>
        {!loading &&
          dates.map((date, index) =>
            date !== 'weekend' ? (
              <ScheduleTable
                onClick={cellClickHandler}
                date={date}
                key={date}
                employees={employees}
              />
            ) : (
              <div className='weekend-gap' key={`${date + index}`}>
                Weekend
              </div>
            )
          )}
      </div>
    </>
  );
};

export default Schedule;
