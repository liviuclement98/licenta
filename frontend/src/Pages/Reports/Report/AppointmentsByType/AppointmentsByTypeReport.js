import React, { useEffect, useState } from 'react';

import MaterialTable from 'material-table';
import Paper from '@material-ui/core/Paper';
import { TextField } from '@material-ui/core';

import PageHeader from '../../../../Components/Shared/PageHeader/PageHeader';
import axios from 'axios';

const columns = [
  { title: 'Appointment Type', field: 'appointmentType' },
  { title: 'Number', field: 'number' },
];

const AppointmentsByTypeReport = (props) => {
  const [from, setFrom] = useState('2020-10-12');
  const [to, setTo] = useState('2020-12-12');
  const [rows, setRows] = useState([
    {
      appointmentType: 'Instalatii',
      number: 12,
    },
    {
      appointmentType: 'Electrice',
      number: 9,
    },
    {
      appointmentType: 'Lacatuserie',
      number: 23,
    },
  ]);

  useEffect(() => {
    // axios
    //   .get('')
    //   .then((res) => {
    //     console.log(res);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });
  }, [from, to]);

  return (
    <>
      <PageHeader title='Appointments by Type Report'>
        <div className='from-date'>
          <TextField
            id='date'
            label='From'
            variant='outlined'
            type='date'
            defaultValue={from}
            onChange={(event) => setFrom(event.target.value)}
            InputLabelProps={{
              shrink: true,
            }}
            style={{ marginRight: 20 }}
          />
        </div>
        <div className='to-date'>
          <TextField
            id='date'
            label='To'
            variant='outlined'
            type='date'
            defaultValue={to}
            onChange={(event) => setTo(event.target.value)}
            InputLabelProps={{
              shrink: true,
            }}
          />
        </div>
      </PageHeader>
      <Paper style={{ marginBottom: '2rem' }}>
        <MaterialTable
          title={`Worker Earning Report from ${from} to ${to}`}
          columns={columns}
          data={rows}
          options={{
            exportButton: true,
            showTitle: false,
            sorting: true,
            searchFieldAlignment: 'left',
          }}
        />
      </Paper>
    </>
  );
};

export default AppointmentsByTypeReport;
