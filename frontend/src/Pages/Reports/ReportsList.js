import React, { useState } from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import './ReportsList.css';

import PageHeader from '../../Components/Shared/PageHeader/PageHeader';
import SearchIcon from '@material-ui/icons/Search';

import { TextField } from '@material-ui/core';

const reports = [
  { title: 'Castiguri Tehnicieni', navLink: '/workers-earnings' },
  { title: 'Lucrari Dupa Tip', navLink: '/appointments-by-type' },
];

const Reports = (props) => {
  let history = useHistory();
  const [reportsToShow, setReportsToShow] = useState(reports);

  const onFilterChangeHandler = (filter) => {
    let filteredReports = [...reports];
    filteredReports = filteredReports.filter((report) =>
      report.title.toLowerCase().includes(filter)
    );
    setReportsToShow(filteredReports);
  };

  return (
    <div className='reports'>
      <PageHeader title='Reports'>
        <div className='reports-search'>
          <SearchIcon style={{ marginRight: 10 }} />
          <TextField
            style={{ width: 300 }}
            onChange={(event) => onFilterChangeHandler(event.target.value)}
          />
        </div>
      </PageHeader>
      <div className='reports-container'>
        {reportsToShow.map((report) => (
          <div
            key={report.navLink}
            className='report'
            onClick={() => history.push(report.navLink)}
          >
            {report.title}
          </div>
        ))}
      </div>
    </div>
  );
};

export default Reports;
