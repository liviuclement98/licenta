import React, { useEffect, useState } from 'react';
import './Users.css';
import Backdrop from '../../../Components/Shared/Backdrop/Backdrop';

import { Button } from '@material-ui/core';

import AddUserModal from '../../../Components/Admin/Users/Modals/AddUserModal';
import ViewUserModal from '../../../Components/Admin/Users/Modals/ViewUserModal';

import UsersTable from '../../../Components/Admin/Users/Tables/UsersTable';
import PageHeader from '../../../Components/Shared/PageHeader/PageHeader';
import axios from 'axios';

import { getConvertedDate } from '../../../Utils/Date';
import { lte } from 'lodash';

const columns = [
  { id: 'id', label: 'ID', minWidth: 170 },
  { id: 'username', label: 'Username', minWidth: 170 },
  { id: 'email', label: 'Email', minWidth: 100 },
  {
    id: 'role',
    label: 'Role',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'date_added',
    label: 'Date Added',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  },
];

const Users = (props) => {
  const [showAddUserModal, setShowAddUserModal] = useState(false);
  const [users, setUsers] = useState([]);
  const [showViewUserModal, setShowViewUserModal] = useState(false);
  const [userToShow, setUserToShow] = useState({});

  useEffect(() => {
    axios
      .get(process.env.REACT_APP_BACKEND_URL + '/users')
      .then((res) => {
        console.log(res);
        setUsers(
          res.data.users.map((user) => ({
            id: user._id,
            username: user.username,
            email: user.email,
            role: user.role || '',
            dateAdded: getConvertedDate(user.dateAdded) || '',
          }))
        );
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const viewUserHandler = (user) => {
    console.log(user);
    setUserToShow(user);
    setShowViewUserModal(true);
  };

  const closeModals = () => {
    setShowAddUserModal(false);
    setShowViewUserModal(false);
  };

  const deleteUserHandler = () => {
    axios
      .delete(process.env.REACT_APP_BACKEND_URL + '/users', {
        data: { id: userToShow.id },
      })
      .then((res) => {
        console.log(res);
        setUsers([...users].filter((user) => user.id !== userToShow.id));
      });
  };

  const updateUserHandler = async (newUser) => {
    const { username, email, role } = newUser;

    const response = await axios.patch(
      process.env.REACT_APP_BACKEND_URL + '/users',
      newUser
    );
    const updatedUser = response.data.updatedUser;
    const index = [...users].findIndex((user) => user.id === updatedUser._id);
    const updatedUsers = [...users];
    updatedUsers[index] = { ...updatedUsers[index], username, email, role };

    setUsers(updatedUsers);
  };

  const addUserHandler = (username, email, password, role) => {
    axios
      .post(process.env.REACT_APP_BACKEND_URL + '/users/signup', {
        username,
        password,
        email,
        role,
      })
      .then((res) => {
        console.log(res);
        const updatedRows = [...users];
        updatedRows.push({
          id: res.data.user._id,
          username,
          email,
          role,
          dateAdded: getConvertedDate(res.data.user.dateAdded),
        });
        setUsers(updatedRows);
      });
  };
  return (
    <>
      {(showAddUserModal || showViewUserModal) && (
        <Backdrop onClick={closeModals} />
      )}
      {showAddUserModal && (
        <AddUserModal onClose={closeModals} onAdd={addUserHandler} />
      )}
      {showViewUserModal && (
        <ViewUserModal
          userToShow={userToShow}
          onClose={closeModals}
          onDelete={deleteUserHandler}
          onUpdate={updateUserHandler}
        />
      )}
      <div className='users'>
        <PageHeader title='Users'>
          <Button
            variant='contained'
            color='primary'
            onClick={() => setShowAddUserModal(true)}
          >
            Add
          </Button>
        </PageHeader>
        <UsersTable
          rows={users}
          headCells={columns}
          viewUserHandler={viewUserHandler}
        />
      </div>
    </>
  );
};

export default Users;
