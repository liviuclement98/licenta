import React, { useState, useEffect } from 'react';
import PageHeader from '../../../Components/Shared/PageHeader/PageHeader';
import { TextField } from '@material-ui/core';

import './Settings.css';
import axios from 'axios';

const Settings = (props) => {
  const [settings, setSettings] = useState({});

  useEffect(() => {
    axios
      .get(process.env.REACT_APP_BACKEND_URL + '/settings')
      .then((res) => {
        console.log(res);

        const fetchedSettings = {};
        res.data.settings.forEach((setting) => {
          fetchedSettings[setting.key] = setting.value;
        });
        setSettings(fetchedSettings);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const saveSetting = (key, value) => {
    axios
      .patch(process.env.REACT_APP_BACKEND_URL + '/settings', { key, value })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <>
      <PageHeader title='Settings' />
      <div className='settings-panel'>
        <TextField
          label='Calendar color:'
          variant='outlined'
          value={settings['calendar_color']}
          onChange={(event) => {
            setSettings({
              ...settings,
              calendar_color: event.target.value,
            });
          }}
          onBlur={() =>
            saveSetting('calendar_color', settings['calendar_color'])
          }
          InputLabelProps={{
            shrink: true,
          }}
          style={{ marginRight: 20, width: 300 }}
        />
        {/* <TextField
          label='Calendar days after current date'
          variant='outlined'
          value={parseInt(settings['calendar_days_after_current'])}
          type='number'
          onChange={(event) => {
            setSettings({
              ...settings,
              calendar_days_after_current: event.target.value,
            });
          }}
          onBlur={() =>
            saveSetting(
              'calendar_days_after_current',
              settings['calendar_days_after_current']
            )
          }
          InputLabelProps={{
            shrink: true,
          }}
          style={{ marginRight: 20, width: 300 }}
        /> */}
      </div>
    </>
  );
};

export default Settings;
