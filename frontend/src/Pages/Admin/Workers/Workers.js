import React, { useEffect, useState } from 'react';
import PageHeader from '../../../Components/Shared/PageHeader/PageHeader';
import { Button } from '@material-ui/core';

import Backdrop from '../../../Components/Shared/Backdrop/Backdrop';
import WorkersTable from '../../../Components/Admin/Workers/Tables/WorkersTable';
import AddWorkerModal from '../../../Components/Admin/Workers/Modals/AddWorkerModal';
import ViewWorkerModal from '../../../Components/Admin/Workers/Modals/ViewWorkerModal';
import axios from 'axios';
import { getConvertedDate } from '../../../Utils/Date';

const columns = [
  { id: 'id', label: 'ID', minWidth: 170 },
  { id: 'name', label: 'Name', minWidth: 170 },
  { id: 'phone', label: 'Phone', minWidth: 100 },
  {
    id: 'address',
    label: 'Address',
    minWidth: 170,
  },
  {
    id: 'date_added',
    label: 'Date Added',
    minWidth: 170,
  },
];

const Workers = (props) => {
  const [showViewWorkerModal, setShowViewWorkerModal] = useState(false);
  const [showAddWorkerModal, setShowAddWorkerModal] = useState(false);
  const [workers, setWorkers] = useState([]);
  const [workerToShow, setWorkerToShow] = useState({});

  useEffect(() => {
    axios
      .get(process.env.REACT_APP_BACKEND_URL + '/workers')
      .then((res) => {
        console.log(res);
        setWorkers(
          res.data.workers.map((worker) => ({
            id: worker._id,
            name: worker.name,
            phone: worker.phone,
            address: worker.address,
            dateAdded: getConvertedDate(worker.dateAdded) || '',
          }))
        );
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const viewWorkerHandler = (worker) => {
    setWorkerToShow(worker);
    setShowViewWorkerModal(true);
  };

  const closeModals = () => {
    setShowAddWorkerModal(false);
    setShowViewWorkerModal(false);
  };

  const addWorkerHandler = async (workerToAdd) => {
    const response = await axios.post(
      process.env.REACT_APP_BACKEND_URL + '/workers',
      workerToAdd
    );
    console.log(response);
    const newWorker = response.data.newWorker;

    console.log(workerToAdd);
    setWorkers([
      ...workers,
      {
        id: newWorker._id,
        ...workerToAdd,
        dateAdded: getConvertedDate(newWorker.dateAdded),
      },
    ]);
  };

  const deleteWorkerHandler = () => {
    axios
      .delete(process.env.REACT_APP_BACKEND_URL + '/workers', {
        data: { id: workerToShow.id },
      })
      .then((res) => {
        console.log(res);
        setWorkers([...workers].filter((user) => user.id !== workerToShow.id));
      });
  };

  const updateWorkerHandler = async (workerToUpdate) => {
    const { username, address, phone } = workerToUpdate;

    const response = await axios.patch(
      process.env.REACT_APP_BACKEND_URL + '/workers',
      workerToUpdate
    );
    console.log(response);
    const updatedWorker = response.data.updatedWorker;
    const index = [...workers].findIndex(
      (user) => user.id === updatedWorker._id
    );
    const updatedWorkers = [...workers];
    updatedWorkers[index] = {
      ...updatedWorkers[index],
      username,
      address,
      phone,
    };

    setWorkers(updatedWorkers);
  };

  return (
    <>
      {(showAddWorkerModal || showViewWorkerModal) && (
        <Backdrop onClick={closeModals} />
      )}
      {showAddWorkerModal && (
        <AddWorkerModal
          onClose={() => setShowAddWorkerModal(false)}
          onAdd={addWorkerHandler}
        />
      )}
      {showViewWorkerModal && (
        <ViewWorkerModal
          onClose={() => setShowViewWorkerModal(false)}
          onUpdate={updateWorkerHandler}
          onDelete={deleteWorkerHandler}
          workerToShow={workerToShow}
        />
      )}
      <PageHeader title='Workers'>
        <Button
          variant='contained'
          color='primary'
          onClick={() => setShowAddWorkerModal(true)}
        >
          Add
        </Button>
      </PageHeader>
      <WorkersTable
        headCells={columns}
        rows={workers}
        viewWorkerHandler={viewWorkerHandler}
      />
    </>
  );
};

export default Workers;
