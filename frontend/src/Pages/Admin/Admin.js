import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import PageHeader from '../../Components/Shared/PageHeader/PageHeader';
import './Admin.css';

import SearchIcon from '@material-ui/icons/Search';

import { TextField } from '@material-ui/core';

const items = [
  { title: 'Users', navLink: '/users' },
  {
    title: 'Settings',
    navLink: '/settings',
  },
  {
    title: 'Workers',
    navLink: '/workers',
  },
];

const Admin = (props) => {
  let history = useHistory();
  const [itemsToShow, setItemsToShow] = useState(items);

  const onFilterChangeHandler = (filter) => {
    let filteredItems = [...items];
    filteredItems = filteredItems.filter((item) =>
      item.title.toLowerCase().includes(filter)
    );
    setItemsToShow(filteredItems);
  };
  return (
    <div className='admin'>
      <PageHeader title='Admin'>
        <div className='items-search'>
          <SearchIcon style={{ marginRight: 10 }} />
          <TextField
            style={{ width: 300 }}
            onChange={(event) => onFilterChangeHandler(event.target.value)}
          />
        </div>
      </PageHeader>
      <div className='admin-container'>
        {itemsToShow.map((item) => (
          <div
            key={item.navLink}
            className='item'
            onClick={() => history.push(item.navLink)}
          >
            {item.title}
          </div>
        ))}
      </div>
    </div>
  );
};

export default Admin;
