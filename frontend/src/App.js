import './App.css';
import MainNavigation from './Components/Shared/Navigation/MainNavigation';
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from 'react-router-dom';
import Admin from './Pages/Admin/Admin';
import Login from './Pages/Auth/Login/Login';
import Clients from './Pages/Clients/Clients';
import ReportsList from './Pages/Reports/ReportsList';
import WorkersEarningsReport from './Pages/Reports/Report/WorkersEarnings/WorkersEarningsReport';
import AppointmentsByTypeReport from './Pages/Reports/Report/AppointmentsByType/AppointmentsByTypeReport';
import Schedule from './Pages/Schedule/Schedule';
import ViewClient from './Pages/Clients/ViewClient/ViewClient';
import Users from './Pages/Admin/Users/Users';
import Workers from './Pages/Admin/Workers/Workers';
import Settings from './Pages/Admin/Settings/Settings';

import { useSelector } from 'react-redux';

function App() {
  const logged = useSelector((state) => state);
  console.log(logged);

  const routes = logged ? (
    <Switch>
      <Route path='/' exact>
        <Schedule />
      </Route>
      <Route path='/clients'>
        <Clients />
      </Route>
      <Route path='/view-client/:id'>
        <ViewClient />
      </Route>
      <Route path='/reports'>
        <ReportsList />
      </Route>
      <Route path='/workers-earnings'>
        <WorkersEarningsReport />
      </Route>
      <Route path='/appointments-by-type'>
        <AppointmentsByTypeReport />
      </Route>
      <Route path='/admin'>
        <Admin />
      </Route>
      <Route path='/users'>
        heroku --version
        <Users />
      </Route>
      <Route path='/workers'>
        <Workers />
      </Route>
      <Route path='/settings'>
        <Settings />
      </Route>
      <Redirect to='/' />
    </Switch>
  ) : (
    <Switch>
      <Route path='/login' exact>
        <Login />
      </Route>
      <Redirect to='/login' />
    </Switch>
  );

  return (
    <div className='App'>
      <Router>
        <MainNavigation />
        <main>{routes}</main>
      </Router>
    </div>
  );
}

export default App;
