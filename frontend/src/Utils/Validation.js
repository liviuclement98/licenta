export const isEmail = (email) => {
  const re = /\S+@\S+\.\S+/;
  return re.test(email);
};

export const isMinLength = (value, min) => {
  return value.toString().length >= min;
};

export const isMaxLength = (value, max) => {
  return value.toString().length <= max;
};

export const isNotNull = (value) => {
  return !!value;
};

export const isLength = (value, length) => {
  return value.toString().length === parseInt(length);
};
