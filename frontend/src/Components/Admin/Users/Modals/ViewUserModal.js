import React, { useEffect, useState } from 'react';
import './ViewUserModal.css';

import { Button, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { add } from 'lodash';
import { isEmail, isMinLength, isNotNull } from '../../../../Utils/Validation';

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

const AddUserModal = (props) => {
  const classes = useStyles();

  const userData = props.userToShow;

  const [username, setUsername] = useState({
    value: userData.username,
    error: false,
  });
  const [email, setEmail] = useState({
    value: userData.email,
    error: false,
  });

  const [role, setRole] = useState({
    value: userData.role,
    error: false,
  });

  const verifyBeforeAdd = () => {
    return !email.error && !username.error && !role.error;
  };

  return (
    <div className='view_user_modal_container'>
      <div className='header'>
        <p className='modal_title'>Add User</p>
        <div className='close_modal' onClick={props.onClose}>
          ✕
        </div>
      </div>
      <div className='modal_body'>
        <div className='input_fields'>
          <TextField
            label='Username'
            variant='outlined'
            defaultValue={userData.username}
            error={username.error && username.isTouched}
            onBlur={() => setUsername({ ...username, isTouched: true })}
            onChange={(event) =>
              setUsername({
                ...username,
                value: event.target.value,
                error: !isMinLength(event.target.value, 6),
              })
            }
          />
          <TextField
            label='Email'
            variant='outlined'
            defaultValue={userData.email}
            error={email.error && email.isTouched}
            onBlur={() => setEmail({ ...email, isTouched: true })}
            onChange={(event) =>
              setEmail({
                ...email,
                value: event.target.value,
                error: !isEmail(event.target.value),
              })
            }
          />
          <TextField
            label='Role'
            type='text'
            variant='outlined'
            defaultValue={userData.role}
            error={role.error && role.isTouched}
            onBlur={() => setRole({ ...role, isTouched: true })}
            onChange={(event) =>
              setRole({
                ...role,
                value: event.target.value,
                error: !isNotNull(event.target.value),
              })
            }
          />
        </div>
      </div>
      <div className='modal_footer'>
        <Button
          variant='contained'
          color='secondary'
          className={classes.margin}
          onClick={() => {
            props.onDelete();
            props.onClose();
          }}
        >
          Delete User
        </Button>
        <Button
          variant='contained'
          color='primary'
          onClick={() => {
            if (verifyBeforeAdd()) {
              props.onUpdate({
                id: userData.id,
                username: username.value,
                email: email.value,
                role: role.value,
              });
              props.onClose();
            }
          }}
        >
          Update User
        </Button>
      </div>
    </div>
  );
};

export default AddUserModal;
