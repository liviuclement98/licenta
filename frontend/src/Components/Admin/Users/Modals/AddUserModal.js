import React, { useEffect, useState } from 'react';
import './AddUserModal.css';

import { Button, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { add } from 'lodash';
import { isEmail, isMinLength, isNotNull } from '../../../../Utils/Validation';

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

const AddUserModal = (props) => {
  const classes = useStyles();

  const [username, setUsername] = useState({
    value: '',
    error: true,
    isTouched: false,
  });
  const [email, setEmail] = useState({
    value: '',
    error: true,
    isTouched: false,
  });
  const [password, setPassword] = useState({
    value: '',
    error: true,
    isTouched: false,
  });
  const [role, setRole] = useState({
    value: '',
    error: true,
    isTouched: false,
  });

  const verifyBeforeAdd = () => {
    setEmail({ ...email, isTouched: true });
    setUsername({ ...username, isTouched: true });
    setPassword({ ...password, isTouched: true });
    setRole({ ...role, isTouched: true });

    return !email.error && !username.error && !password.error && !role.error;
  };

  return (
    <div className='add_user_modal_container'>
      <div className='header'>
        <p className='modal_title'>Add User</p>
        <div className='close_modal' onClick={props.onClose}>
          ✕
        </div>
      </div>
      <div className='modal_body'>
        <div className='input_fields'>
          <TextField
            label='Username'
            variant='outlined'
            error={username.error && username.isTouched}
            onBlur={() => setUsername({ ...username, isTouched: true })}
            onChange={(event) =>
              setUsername({
                ...username,
                value: event.target.value,
                error: !isMinLength(event.target.value, 6),
              })
            }
          />
          <TextField
            label='Email'
            variant='outlined'
            error={email.error && email.isTouched}
            onBlur={() => setEmail({ ...email, isTouched: true })}
            onChange={(event) =>
              setEmail({
                ...email,
                value: event.target.value,
                error: !isEmail(event.target.value),
              })
            }
          />
          <TextField
            label='Password'
            type='password'
            variant='outlined'
            error={password.error && password.isTouched}
            onBlur={() => setPassword({ ...password, isTouched: true })}
            onChange={(event) =>
              setPassword({
                ...password,
                value: event.target.value,
                error: !isMinLength(event.target.value, 6),
              })
            }
          />
          <TextField
            label='Role'
            type='text'
            variant='outlined'
            error={role.error && role.isTouched}
            onBlur={() => setRole({ ...role, isTouched: true })}
            onChange={(event) =>
              setRole({
                ...role,
                value: event.target.value,
                error: !isNotNull(event.target.value),
              })
            }
          />
        </div>
      </div>
      <div className='modal_footer'>
        <Button
          variant='contained'
          color='secondary'
          className={classes.margin}
          onClick={props.onClose}
        >
          Cancel
        </Button>
        <Button
          variant='contained'
          color='primary'
          onClick={() => {
            if (verifyBeforeAdd()) {
              props.onAdd(
                username.value,
                email.value,
                password.value,
                role.value
              );
              props.onClose();
            }
          }}
        >
          Add User
        </Button>
      </div>
    </div>
  );
};

export default AddUserModal;
