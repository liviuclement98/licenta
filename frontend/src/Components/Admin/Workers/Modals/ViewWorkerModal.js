import React, { useEffect, useState } from 'react';
import './ViewWorkerModal.css';

import { Button, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { add } from 'lodash';
import { isLength, isNotNull } from '../../../../Utils/Validation';

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

const ViewWorkerModal = (props) => {
  const classes = useStyles();

  const workerData = props.workerToShow;

  const [name, setName] = useState({
    value: workerData.name,
    error: false,
  });
  const [address, setAddress] = useState({
    value: workerData.address,
    error: false,
  });
  const [phone, setPhone] = useState({
    value: workerData.phone,
    error: false,
  });

  const verifyBeforeAdd = () => {
    return !name.error && !address.error && !phone.error && !address.error;
  };

  return (
    <div className='view_worker_modal_container'>
      <div className='header'>
        <p className='modal_title'>View Worker</p>
        <div className='close_modal' onClick={props.onClose}>
          ✕
        </div>
      </div>
      <div className='modal_body'>
        <div className='input_fields'>
          <TextField
            label='Name'
            variant='outlined'
            defaultValue={workerData.name}
            error={name.error && name.isTouched}
            onBlur={() => setName({ ...name, isTouched: true })}
            onChange={(event) =>
              setName({
                ...name,
                value: event.target.value,
                error: !isNotNull(event.target.value),
              })
            }
          />
          <TextField
            label='Address'
            variant='outlined'
            defaultValue={workerData.address}
            error={address.error && address.isTouched}
            onBlur={() => setAddress({ ...address, isTouched: true })}
            onChange={(event) =>
              setAddress({
                ...address,
                value: event.target.value,
                error: !isNotNull(event.target.value),
              })
            }
          />
          <TextField
            label='Phone'
            type='number'
            variant='outlined'
            defaultValue={workerData.phone}
            error={phone.error && phone.isTouched}
            onBlur={() => setPhone({ ...phone, isTouched: true })}
            onChange={(event) =>
              setPhone({
                ...phone,
                value: event.target.value,
                error: !isLength(event.target.value, 10),
              })
            }
          />
        </div>
      </div>
      <div className='modal_footer'>
        <Button
          variant='contained'
          color='secondary'
          className={classes.margin}
          onClick={() => {
            props.onDelete();
            props.onClose();
          }}
        >
          Delete Worker
        </Button>
        <Button
          variant='contained'
          color='primary'
          onClick={() => {
            if (verifyBeforeAdd()) {
              props.onUpdate({
                id: workerData.id,
                name: name.value,
                phone: phone.value,
                address: address.value,
              });
              props.onClose();
            }
          }}
        >
          Update Worker
        </Button>
      </div>
    </div>
  );
};

export default ViewWorkerModal;
