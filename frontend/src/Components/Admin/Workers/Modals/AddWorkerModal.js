import React, { useEffect, useState } from 'react';
import './AddWorkerModal.css';

import { Button, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { add } from 'lodash';
import { isLength, isNotNull } from '../../../../Utils/Validation';

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

const AddWorkerModal = (props) => {
  const classes = useStyles();

  const [name, setName] = useState({
    value: '',
    error: true,
    isTouched: false,
  });
  const [address, setAddress] = useState({
    value: '',
    error: true,
    isTouched: false,
  });
  const [phone, setPhone] = useState({
    value: '',
    error: true,
    isTouched: false,
  });

  const verifyBeforeAdd = () => {
    setAddress({ ...address, isTouched: true });
    setPhone({ ...phone, isTouched: true });
    setName({ ...name, isTouched: true });
    return !name.error && !address.error && !phone.error && !address.error;
  };

  return (
    <div className='add_worker_modal_container'>
      <div className='header'>
        <p className='modal_title'>Add Worker</p>
        <div className='close_modal' onClick={props.onClose}>
          ✕
        </div>
      </div>
      <div className='modal_body'>
        <div className='input_fields'>
          <TextField
            label='Name'
            variant='outlined'
            error={name.error && name.isTouched}
            onBlur={() => setName({ ...name, isTouched: true })}
            onChange={(event) =>
              setName({
                ...name,
                value: event.target.value,
                error: !isNotNull(event.target.value),
              })
            }
          />
          <TextField
            label='Address'
            variant='outlined'
            error={address.error && address.isTouched}
            onBlur={() => setAddress({ ...address, isTouched: true })}
            onChange={(event) =>
              setAddress({
                ...address,
                value: event.target.value,
                error: !isNotNull(event.target.value),
              })
            }
          />
          <TextField
            label='Phone'
            type='number'
            variant='outlined'
            error={phone.error && phone.isTouched}
            onBlur={() => setPhone({ ...phone, isTouched: true })}
            onChange={(event) =>
              setPhone({
                ...phone,
                value: event.target.value,
                error: !isLength(event.target.value, 10),
              })
            }
          />
        </div>
      </div>
      <div className='modal_footer'>
        <Button
          variant='contained'
          color='secondary'
          className={classes.margin}
          onClick={props.onClose}
        >
          Cancel
        </Button>
        <Button
          variant='contained'
          color='primary'
          onClick={() => {
            if (verifyBeforeAdd()) {
              props.onAdd({
                name: name.value,
                phone: phone.value,
                address: address.value,
              });
              props.onClose();
            }
          }}
        >
          Add Worker
        </Button>
      </div>
    </div>
  );
};

export default AddWorkerModal;
