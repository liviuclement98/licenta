import React, { useEffect, useState } from 'react';
import './EditClientModal.css';

import { Button, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { add } from 'lodash';
import { isEmail, isLength, isNotNull } from '../../../Utils/Validation';

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

const AddClientModal = (props) => {
  const classes = useStyles();

  const [company, setCompany] = useState(props.data.company);
  const [name, setName] = useState({
    value: props.data.name,
    error: false,
  });
  const [phoneNr, setPhoneNr] = useState({
    value: props.data.phone,
    error: false,
  });
  const [email, setEmail] = useState({
    value: props.data.email,
    error: false,
  });
  const [address, setAddress] = useState({
    value: props.data.address,
    error: false,
  });

  const verifyBeforeAdd = () => {
    return !email.error && !address.error && !name.error && !phoneNr.error;
  };

  return (
    <div className='add_client_modal_container'>
      <div className='header'>
        <p className='modal_title'>Edit Client</p>
        <div className='close_modal' onClick={props.onClose}>
          ✕
        </div>
      </div>
      <div className='modal_body'>
        <div className='input_fields'>
          <TextField
            label='Company'
            variant='outlined'
            value={company}
            onChange={(event) => setCompany(event.target.value)}
          />
          <TextField
            label='Name'
            variant='outlined'
            value={name.value}
            error={name.error}
            onChange={(event) =>
              setName({
                ...name,
                value: event.target.value,
                error: !isNotNull(event.target.value),
              })
            }
          />
          <TextField
            label='Phone'
            type='number'
            variant='outlined'
            value={phoneNr.value}
            error={phoneNr.error}
            onChange={(event) =>
              setPhoneNr({
                ...phoneNr,
                value: event.target.value,
                error: !isLength(event.target.value, 10),
              })
            }
          />
          <TextField
            label='Email'
            type='email'
            variant='outlined'
            value={email.value}
            error={email.error}
            onChange={(event) =>
              setEmail({
                ...email,
                value: event.target.value,
                error: !isEmail(event.target.value),
              })
            }
          />
          <TextField
            label='Address'
            value={address.value}
            error={address.error}
            variant='outlined'
            onChange={(event) =>
              setAddress({
                ...address,
                value: event.target.value,
                error: !isNotNull(event.target.value),
              })
            }
          />
        </div>
      </div>
      <div className='modal_footer'>
        <Button
          variant='contained'
          color='secondary'
          className={classes.margin}
          onClick={props.onClose}
        >
          Cancel
        </Button>
        <Button
          variant='contained'
          color='primary'
          onClick={() => {
            if (verifyBeforeAdd()) {
              props.onUpdate(
                company,
                name.value,
                phoneNr.value,
                email.value,
                address.value
              );
              props.onClose();
            }
          }}
        >
          Update Client
        </Button>
      </div>
    </div>
  );
};

export default AddClientModal;
