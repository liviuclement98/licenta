import React, { useEffect, useState } from 'react';
import './AddClientModal.css';

import { Button, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { add } from 'lodash';
import { isEmail, isLength, isNotNull } from '../../../Utils/Validation';

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

const AddClientModal = (props) => {
  const classes = useStyles();

  const [company, setCompany] = useState();
  const [name, setName] = useState({
    value: '',
    error: true,
    isTouched: false,
  });
  const [phoneNr, setPhoneNr] = useState({
    value: '',
    error: true,
    isTouched: false,
  });
  const [email, setEmail] = useState({
    value: '',
    error: true,
    isTouched: false,
  });
  const [address, setAddress] = useState({
    value: '',
    error: true,
    isTouched: false,
  });

  const verifyBeforeAdd = () => {
    setEmail({ ...email, isTouched: true });
    setAddress({ ...address, isTouched: true });
    setName({ ...name, isTouched: true });
    setPhoneNr({ ...phoneNr, isTouched: true });

    return !email.error && !address.error && !name.error && !phoneNr.error;
  };

  return (
    <div className='add_client_modal_container'>
      <div className='header'>
        <p className='modal_title'>Add Client</p>
        <div className='close_modal' onClick={props.onClose}>
          ✕
        </div>
      </div>
      <div className='modal_body'>
        <div className='input_fields'>
          <TextField
            label='Company'
            variant='outlined'
            onChange={(event) => setCompany(event.target.value)}
          />
          <TextField
            label='Name'
            variant='outlined'
            error={name.error && name.isTouched}
            onBlur={() => setName({ ...name, isTouched: true })}
            onChange={(event) =>
              setName({
                ...name,
                value: event.target.value,
                error: !isNotNull(event.target.value),
              })
            }
          />
          <TextField
            label='Phone'
            type='number'
            variant='outlined'
            error={phoneNr.error && phoneNr.isTouched}
            onBlur={() => setName({ ...phoneNr, isTouched: true })}
            onChange={(event) =>
              setPhoneNr({
                ...phoneNr,
                value: event.target.value,
                error: !isLength(event.target.value, 10),
              })
            }
          />
          <TextField
            label='Email'
            type='email'
            variant='outlined'
            error={email.error && email.isTouched}
            onBlur={() => setName({ ...email, isTouched: true })}
            onChange={(event) =>
              setEmail({
                ...email,
                value: event.target.value,
                error: !isEmail(event.target.value),
              })
            }
          />
          <TextField
            label='Address'
            error={address.error && address.isTouched}
            variant='outlined'
            onBlur={() => setName({ ...address, isTouched: true })}
            onChange={(event) =>
              setAddress({
                ...address,
                value: event.target.value,
                error: !isNotNull(event.target.value),
              })
            }
          />
        </div>
      </div>
      <div className='modal_footer'>
        <Button
          variant='contained'
          color='secondary'
          className={classes.margin}
          onClick={props.onClose}
        >
          Cancel
        </Button>
        <Button
          variant='contained'
          color='primary'
          onClick={() => {
            if (verifyBeforeAdd()) {
              props.onAdd(
                company,
                name.value,
                phoneNr.value,
                email.value,
                address.value
              );
              props.onClose();
            }
          }}
        >
          Add Client
        </Button>
      </div>
    </div>
  );
};

export default AddClientModal;
