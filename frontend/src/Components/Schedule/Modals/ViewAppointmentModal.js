import React, { useState } from 'react';
import './AppointmentModal.css';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Button } from '@material-ui/core';
import {
  createMuiTheme,
  withStyles,
  makeStyles,
  ThemeProvider,
} from '@material-ui/core/styles';
import {
  green,
  purple,
  blue,
  orange,
  deepOrange,
  lightBlue,
} from '@material-ui/core/colors';
import EmployeeHoursCard from './EmployeeHoursCard';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import DatePicker from 'react-multi-date-picker';
import DatePanel from 'react-multi-date-picker/plugins/date_panel';
import lodash from 'lodash';
const SERVICE_TYPES = ['instalatii', 'electricitate', 'termopane'];
const SERVICE_STATUSES = ['deschisa', 'facturata', 'inchisa', 'evaluare'];
const DURATION = [
  {
    label: '1',
    value: 1,
  },
  {
    label: '1.5',
    value: 1.5,
  },
  {
    label: '2',
    value: 2,
  },
  {
    label: '2.5',
    value: 2.5,
  },
  {
    label: '3',
    value: 3,
  },
  {
    label: '3.5',
    value: 3.5,
  },
  {
    label: '4',
    value: 4,
  },
  {
    label: '4.5',
    value: 4.5,
  },
  {
    label: '5',
    value: 5,
  },
  {
    label: '5.5',
    value: 5.5,
  },
  {
    label: '6',
    value: 6,
  },
  {
    label: '6.5',
    value: 6.5,
  },
  {
    label: '7',
    value: 7,
  },
  {
    label: '7.5',
    value: 7.5,
  },
  {
    label: '8',
    value: 8,
  },
];

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography component={'span'}>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

const ViewAppointmentModal = (props) => {
  const { appointment } = props;
  const classes = useStyles();

  const datesEmployeesCopy = lodash.cloneDeep(appointment.datesEmployees);

  const [companyName, setCompanyName] = useState(appointment.company);
  const [description, setDescription] = useState(appointment.description);
  const [serviceType, setServiceType] = useState(appointment.type);
  const [serviceStatus, setServiceStatus] = useState(appointment.status);
  const [contactName, setContactName] = useState(appointment.name);
  const [phoneNr, setPhoneNr] = useState(appointment.phone);
  const [email, setEmail] = useState(appointment.email);
  const [datesEmployees, setDatesEmployees] = useState(datesEmployeesCopy);
  const [address, setAddress] = useState(appointment.address);
  const [comunicatedPrice, setComunicatedPrice] = useState(
    appointment.comunicatedPrice
  );
  const [chargedPrice, setChargedPrice] = useState(appointment.chargedPrice);
  const [observations, setObservations] = useState(appointment.observations);
  const [tabValue, setTabValue] = useState(0);

  const clipboardText = `description: ${description}`;

  const handleTabsChange = (event, newValue) => {
    setTabValue(newValue);
  };

  const employeesChangeHandler = (event, newEmployees) => {
    if (datesEmployees.length === 0) return;

    const newDatesEmployees = [...datesEmployees];

    if (newEmployees.length === 0) {
      newDatesEmployees[tabValue] = {
        ...newDatesEmployees[tabValue],
        employeesTimes: [],
      };
      return setDatesEmployees(newDatesEmployees);
    }

    const newEmployeeIds = newEmployees.map((newEmployee) => newEmployee.id);

    const currentEmployeeIds = newDatesEmployees[tabValue].employeesTimes.map(
      (employeeTimes) => employeeTimes.id
    );

    if (newEmployeeIds.length > currentEmployeeIds.length) {
      for (const newEmployeeId in newEmployeeIds) {
        const index = currentEmployeeIds.indexOf(newEmployeeIds[newEmployeeId]);
        if (index === -1) {
          newDatesEmployees[tabValue].employeesTimes.push({
            id: newEmployees[newEmployeeId].id,
            name: newEmployees[newEmployeeId].name,
            startTime: props.startTime,
            duration: null,
          });
          break;
        }
      }
    } else {
      for (const currentEmployeeId in currentEmployeeIds) {
        const index = newEmployeeIds.indexOf(
          currentEmployeeIds[currentEmployeeId]
        );

        if (index === -1) {
          newDatesEmployees[tabValue].employeesTimes.splice(
            currentEmployeeId,
            1
          );
          break;
        }
      }
    }

    setDatesEmployees(newDatesEmployees);
  };

  const datesChangeHandler = (dateObjs) => {
    if (dateObjs.length === 0) {
      return setDatesEmployees([]);
    }

    let tabDeleted;
    let newDatesEmployees = [...datesEmployees];

    const currentDates = datesEmployees.map(
      (dateEmployee) => dateEmployee.date
    );

    const newDates = dateObjs.map((dateObj) => {
      const month =
        dateObj.month.number < 10
          ? `0${dateObj.month.number}`
          : dateObj.month.number;
      const day = dateObj.day < 10 ? `0${dateObj.day}` : dateObj.day;

      return `${day}-${month}-${dateObj.year}`;
    });

    //If new objects array's length is hisgher than the old one
    //check from new arr to old arr, else the other way around
    if (dateObjs.length > datesEmployees.length) {
      for (const newDate of newDates) {
        const index = currentDates.indexOf(newDate);
        if (index === -1) {
          newDatesEmployees.push({
            date: newDate,
            employeesTimes: [],
          });

          break;
        }
      }
    } else {
      for (const currentDate in currentDates) {
        const index = newDates.indexOf(currentDates[currentDate]);

        if (index === -1) {
          newDatesEmployees.splice(currentDate, 1);
          tabDeleted = currentDate;
          break;
        }
      }
    }

    setDatesEmployees(newDatesEmployees);

    if (tabValue >= tabDeleted && tabValue > 0) {
      setTabValue(tabValue - 1);
    }
  };

  const startTimeChangeHandler = (event, employeeId) => {
    const newDatesEmployees = [...datesEmployees];

    const index = newDatesEmployees[tabValue].employeesTimes.findIndex(
      (employeeTime) => employeeTime.id === employeeId
    );

    newDatesEmployees[tabValue].employeesTimes[index].startTime =
      event.target.value;

    setDatesEmployees(newDatesEmployees);
  };

  const durationChangeHandler = (newValue, employeeId) => {
    const newDatesEmployees = [...datesEmployees];

    const index = newDatesEmployees[tabValue].employeesTimes.findIndex(
      (employeeTime) => employeeTime.id === employeeId
    );

    newDatesEmployees[tabValue].employeesTimes[index].duration = newValue;

    setDatesEmployees(newDatesEmployees);
  };

  return (
    <div className='appointment_modal_container'>
      <div className='header'>
        <p className='modal_title'>View Appointment</p>
        <div
          className='close_modal'
          onClick={() => {
            setDatesEmployees([]);
            props.onClose();
          }}
        >
          ✕
        </div>
      </div>
      <div className='modal_body'>
        <div className='input_fields'>
          <div style={{ width: '100%' }}>
            <TextField
              style={{ width: '48%', marginRight: '4%' }}
              label='Nume Firma'
              variant='outlined'
              value={companyName}
              InputProps={{
                readOnly: true,
              }}
            />
            <TextField
              style={{ width: '48%' }}
              label='Email'
              variant='outlined'
              value={email}
              InputProps={{
                readOnly: true,
              }}
            />
          </div>
          <TextField
            id='outlined-multiline-static'
            label='Adresa'
            rows={1}
            value={address}
            InputProps={{
              readOnly: true,
            }}
            variant='outlined'
          />

          <div style={{ width: '100%' }}>
            <TextField
              style={{ width: '48%', marginRight: '4%' }}
              label='Nume Client'
              variant='outlined'
              value={contactName}
              InputProps={{
                readOnly: true,
              }}
            />
            <TextField
              style={{ width: '48%' }}
              label='Telefon'
              variant='outlined'
              value={phoneNr}
              InputProps={{
                readOnly: true,
              }}
            />
          </div>

          <div style={{ width: '100%' }}>
            <TextField
              label='Pret Comunicat (LEI)'
              type='number'
              variant='outlined'
              onChange={(event) => setComunicatedPrice(event.target.value)}
              defaultValue={comunicatedPrice}
              style={{ width: '48%', marginRight: '4%' }}
            />
            <TextField
              label='Pret Incasat (LEI)'
              type='number'
              variant='outlined'
              onChange={(event) => setChargedPrice(event.target.value)}
              defaultValue={chargedPrice}
              style={{ width: '48%' }}
            />
          </div>
          <div style={{ width: '100%', display: 'flex' }}>
            <Autocomplete
              options={SERVICE_TYPES}
              style={{ width: '48%', marginRight: '4%' }}
              defaultValue={serviceType}
              onChange={(event, newValue) => setServiceType(newValue)}
              renderInput={(params) => (
                <TextField {...params} label='Tip Lucrare' variant='outlined' />
              )}
            />
            <Autocomplete
              options={SERVICE_STATUSES}
              defaultValue={serviceStatus}
              onChange={(event, newValue) => setServiceStatus(newValue)}
              style={{ width: '48%' }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label='Status Lucrare'
                  variant='outlined'
                />
              )}
            />
          </div>
          <TextField
            id='outlined-multiline-static'
            label='Descriere Lucrare'
            multiline
            rows={1}
            variant='outlined'
            defaultValue={description}
            onChange={(event) => setDescription(event.target.value)}
          />
          <div
            style={{
              width: '100%',
              display: 'flex',
              justifyContent: 'space-between',
            }}
          >
            <div style={{ width: '90%' }}>
              <Autocomplete
                multiple
                getOptionLabel={(selectedEmployee) => selectedEmployee.name}
                getOptionSelected={(option, value) => value.id === option.id}
                options={props.employees}
                value={
                  datesEmployees[tabValue]?.employeesTimes.map(
                    (employeeTime) => ({
                      id: employeeTime.id,
                      name: employeeTime.name,
                    })
                  ) || []
                }
                defaultValue={datesEmployees[tabValue]?.employeesTimes}
                onChange={employeesChangeHandler}
                renderInput={(params) => (
                  <TextField {...params} variant='outlined' label='Tehnician' />
                )}
              />
            </div>
            <div></div>
            <DatePicker
              value={datesEmployees.map(
                (dateEmployee) =>
                  new Date(dateEmployee.date.split('-').reverse().join('-'))
              )}
              type='icon'
              style={{ alignItems: 'center' }}
              multiple
              plugins={[<DatePanel />]}
              onChange={datesChangeHandler}
            />
          </div>

          <TextField
            id='outlined-multiline-static'
            label='Observatii'
            multiline
            rows={1}
            defaultValue={observations}
            variant='outlined'
            onChange={(event) => setObservations(event.target.value)}
          />
        </div>
        <div className='modal_hours'>
          <div className='hours_container'>
            <div className={`hours_header ${classes.root}`}>
              <AppBar position='static' color='default'>
                <Tabs
                  value={tabValue}
                  onChange={handleTabsChange}
                  indicatorColor='primary'
                  textColor='primary'
                  variant='scrollable'
                  scrollButtons='auto'
                  aria-label='scrollable auto tabs example'
                >
                  {datesEmployees.map((dateEmployees) => (
                    <Tab
                      key={dateEmployees.date}
                      label={dateEmployees.date}
                      {...a11yProps(dateEmployees.date)}
                    />
                  ))}
                </Tabs>
              </AppBar>
            </div>
            {datesEmployees.map((dateEmployees, index) => (
              <TabPanel value={tabValue} index={index} key={dateEmployees.date}>
                <div className={`hours_content`}>
                  {dateEmployees.employeesTimes.map((employeeTimes) => (
                    <EmployeeHoursCard
                      key={`${dateEmployees.date}_${employeeTimes.id}`}
                    >
                      <div style={{ width: '120px' }}>{employeeTimes.name}</div>
                      <TextField
                        label='Ora Incepere'
                        type='time'
                        value={employeeTimes.startTime || ''}
                        InputLabelProps={{
                          shrink: true,
                        }}
                        onChange={(event) =>
                          startTimeChangeHandler(event, employeeTimes.id)
                        }
                        variant='outlined'
                        style={{
                          marginRight: '1rem',
                          marginLeft: '1rem',
                          width: '30%',
                        }}
                        inputProps={{
                          step: 300, // 5 min
                        }}
                      />

                      <Autocomplete
                        value={employeeTimes.duration?.toString()}
                        options={DURATION.map((duration) => duration.label)}
                        defaultValue={employeeTimes.duration || null}
                        style={{ width: '30%' }}
                        onChange={(event, newValue) =>
                          durationChangeHandler(newValue, employeeTimes.id)
                        }
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            label='Durata'
                            variant='outlined'
                          />
                        )}
                      />
                    </EmployeeHoursCard>
                  ))}
                </div>
              </TabPanel>
            ))}
          </div>
        </div>
      </div>
      <div className='modal_footer'>
        <Button
          variant='contained'
          // color='black'
          style={{ marginRight: 10 }}
          onClick={() => navigator.clipboard.writeText(clipboardText)}
        >
          Copy to Clipboard
        </Button>
        <div>
          <Button
            variant='contained'
            color='secondary'
            style={{ marginRight: 10 }}
            onClick={() => {
              props.onDelete();
              props.onClose();
            }}
          >
            Delete Appointment
          </Button>
          <Button
            variant='contained'
            color='primary'
            onClick={() => {
              console.log('dates employees in modal: ', datesEmployees);
              props.onUpdate({
                datesEmployees,
                description,
                serviceType,
                serviceStatus,
                comunicatedPrice,
                chargedPrice,
                observations,
              });
              props.onClose();
            }}
          >
            Update Appointment
          </Button>
        </div>
      </div>
    </div>
  );
};

export default ViewAppointmentModal;
