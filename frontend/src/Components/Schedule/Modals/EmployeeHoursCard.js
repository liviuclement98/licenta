import React from 'react';
import './EmployeeHoursCard.css';

const EmployeeHoursCard = (props) => {
  return <div className='card'>{props.children}</div>;
};

export default EmployeeHoursCard;
