import React, { useRef } from 'react';
import './ScheduleTable.css';
import { HOURS } from '../ScheduleConstants';

const ScheduleTable = (props) => {
  const date = new Date().getDate();

  const tHead = (
    <thead>
      <tr>
        <th className='date-container'>{props.date}</th>
        {HOURS.map((hour) => (
          <th
            id={`${props.date}_${hour.hour}`}
            key={`${props.date}_${hour.hour}`}
          >
            {hour.hour}
          </th>
        ))}
      </tr>
    </thead>
  );

  const tBody = (
    <tbody>
      {props.employees.map((employee) => (
        <tr key={employee.id}>
          <td className='first_cell_in_row'>{employee.name}</td>
          {HOURS.map((hour, i) =>
            i < HOURS.length - 1 ? (
              <td
                onClick={(event) =>
                  props.onClick(
                    employee.id,
                    hour.hour,
                    event.target,
                    props.date
                  )
                }
                key={`${props.date}_${employee.id}_${hour.hour}`}
                id={`${props.date}_${employee.id}_${hour.hour}`}
              ></td>
            ) : null
          )}
        </tr>
      ))}
    </tbody>
  );

  return (
    <div className='schedule-container'>
      {/* <div className='schedule-header'>{props.date}</div> */}
      <div className='scheduler'>
        <table>
          {tHead}
          {tBody}
        </table>
      </div>
    </div>
  );
};

export default ScheduleTable;
