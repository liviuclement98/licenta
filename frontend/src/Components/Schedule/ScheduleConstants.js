export const EMPLOYEES = [
  { id: 1, name: 'Marian Olteanu' },
  { id: 2, name: 'Liviu Zaliznea' },
  { id: 3, name: 'Alex Mihailiuc' },
  { id: 4, name: 'Ionut Ghita' },
  { id: 5, name: 'Marius Radu' },
  { id: 6, name: 'Mihai Udriste' },
  { id: 7, name: 'Cristian Toma' },
];

export const HOURS = [
  { id: '0900', hour: '09:00' },
  { id: '0930', hour: '09:30' },
  { id: '1000', hour: '10:00' },
  { id: '1030', hour: '10:30' },
  { id: '1100', hour: '11:00' },
  { id: '1130', hour: '11:30' },
  { id: '1200', hour: '12:00' },
  { id: '1230', hour: '12:30' },
  { id: '1300', hour: '13:00' },
  { id: '1330', hour: '13:30' },
  { id: '1400', hour: '14:00' },
  { id: '1430', hour: '14:30' },
  { id: '1500', hour: '15:00' },
  { id: '1530', hour: '15:30' },
  { id: '1600', hour: '16:00' },
  { id: '1630', hour: '16:30' },
  { id: '1700', hour: '17:00' },
  { id: '1730', hour: '17:30' },
  { id: '1800', hour: '18:00' },
];
