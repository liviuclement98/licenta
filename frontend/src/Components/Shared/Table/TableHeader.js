import React from 'react';
import './TableHeader.css';

import SearchIcon from '@material-ui/icons/Search';
import TextField from '@material-ui/core/TextField';

const TableHeader = (props) => {
  return (
    <div className='table-header'>
      <SearchIcon style={{ marginRight: 10 }} />

      <TextField
        style={{ width: 300 }}
        onChange={(event) => props.onChange(event.target.value)}
      />
    </div>
  );
};

export default TableHeader;
