import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import './NavLinks.css';

const NavLinks = (props) => {
  const [isAdminDropdownActive, setIsAdminDropdownActive] = useState(false);

  return (
    <ul className='nav-links'>
      <li>
        <NavLink to='/' exact>
          Schedule
        </NavLink>
      </li>
      <li>
        <NavLink to='/clients'>Clients</NavLink>
      </li>
      <li>
        <NavLink to='/reports'>Reports</NavLink>
      </li>
      <li>
        <NavLink to='/admin'>Admin</NavLink>
      </li>
    </ul>
  );
};

export default NavLinks;
