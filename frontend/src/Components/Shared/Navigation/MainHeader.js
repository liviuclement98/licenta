import React, { useState } from 'react';
import './MainHeader.css';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { useDispatch, useSelector } from 'react-redux';
import { logout } from '../../../Redux/Actions/index';

const MainHeader = (props) => {
  const [showProfileDropdown, setShowProfileDropdown] = useState(false);
  const dispatch = useDispatch();
  const logged = useSelector((state) => state);

  const showProfileDropdownChangeHandler = () => {
    if (logged) {
      setShowProfileDropdown(!showProfileDropdown);
    }
  };

  return (
    <div className='main-header-wrapper'>
      <div className='main-header'>
        <div className='header-children'>{props.children}</div>
        <div className='profile-container'>
          <ClickAwayListener onClickAway={() => setShowProfileDropdown(false)}>
            <div className='profile-btn'>
              <AccountCircleIcon
                style={{ width: 40, height: 40 }}
                onClick={showProfileDropdownChangeHandler}
              />
            </div>
          </ClickAwayListener>
          {showProfileDropdown && (
            <ul className='profile-dropdown'>
              <li onClick={() => dispatch(logout())}>
                <ExitToAppIcon style={{ marginRight: 5, color: 'red' }} />{' '}
                LOGOUT
              </li>
            </ul>
          )}
        </div>
      </div>
      ;
    </div>
  );
};

export default MainHeader;
