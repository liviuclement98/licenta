import React, { useState } from 'react';
import MainHeader from './MainHeader';
import { Link } from 'react-router-dom';
import NavLinks from './NavLinks';
import SideDrawer from './SideDrawer';
import './MainNavigation.css';
import Backdrop from '../Backdrop/Backdrop';
import logo from '../../../Assets/reparatot.png';

const MainNavigation = (props) => {
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);

  const openDrawerHandler = () => {
    setIsDrawerOpen(true);
  };

  const closeDrawerHandler = () => {
    setIsDrawerOpen(false);
  };

  return (
    <>
      {isDrawerOpen && <Backdrop onClick={closeDrawerHandler} />}

      <SideDrawer onClick={closeDrawerHandler} show={isDrawerOpen}>
        <NavLinks />
      </SideDrawer>
      <MainHeader>
        <div className='nav-button' onClick={openDrawerHandler}>
          <div className='line'></div>
          <div className='line'></div>
          <div className='line'></div>
        </div>
        <Link to='/' style={{ marginTop: 3 }}>
          <img src={logo} alt='main-logo' className='main-navigation-logo' />
        </Link>

        <nav className='main-navigation-header-nav'>
          <NavLinks />
        </nav>
      </MainHeader>
    </>
  );
};

export default MainNavigation;
