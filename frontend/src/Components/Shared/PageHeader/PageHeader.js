import React from 'react';
import './PageHeader.css';

const PageHeader = (props) => {
  return (
    <div className='page-header'>
      <h1>{props.title}</h1>
      <div className='header-buttons'>{props.children}</div>
    </div>
  );
};

export default PageHeader;
