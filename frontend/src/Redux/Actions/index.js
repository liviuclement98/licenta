export const login = () => {
  return {
    type: 'SIGNIN',
  };
};

export const logout = () => {
  return {
    type: 'LOGOUT',
  };
};
