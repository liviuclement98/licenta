const express = require('express');
const mongoose = require('mongoose');

const clientsRoutes = require('./routes/clients-routes');
const scheduleRoutes = require('./routes/schedule-routes');
const usersRoutes = require('./routes/users-routes');
const settingsRoutes = require('./routes/settings-routes');
const workersRoutes = require('./routes/workers-routes');
require('dotenv').config();

const HttpError = require('./models/http-error');

const app = express();

const cors = require('cors');

app.use(express.json());

// app.use(cors());

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, DELETE');

  next();
});

app.use('/api/clients/', clientsRoutes);

app.use('/api/schedule/', scheduleRoutes);

app.use('/api/users/', usersRoutes);

app.use('/api/settings/', settingsRoutes);

app.use('/api/workers/', workersRoutes);

app.use((req, res, next) => {
  const error = new HttpError('Could not find this route', 404);
  throw error;
});

app.use((error, req, res, next) => {
  if (res.headerSent) {
    return next(error);
  }
  res.status(error.code || 500);
  res.json({ message: error.message || 'An unknown error occured!' });
});

const url = process.env.DB_KEY;

mongoose
  .connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .then(() => {
    app.listen(process.env.PORT || 5000);
  })
  .catch((err) => console.log(err));
