const express = require('express');
const { check } = require('express-validator');

const workersController = require('../controllers/workers-controller');

const router = express.Router();

router.get('/', workersController.getWorkers);
router.get('/:wid', workersController.getWorkerById);

router.post('/', workersController.addWorker);
router.patch('/', workersController.updateWorker);
router.delete('/', workersController.deleteWorker);

module.exports = router;
