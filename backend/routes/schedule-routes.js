const express = require('express');

const scheduleController = require('../controllers/schedule-controller');

const router = express.Router();

router.get('/', scheduleController.getSchedule);

router.post('/', scheduleController.addAppointment);

router.patch('/', scheduleController.updateAppointment);

router.delete('/', scheduleController.deleteAppointment);

module.exports = router;
