const express = require('express');
const { check } = require('express-validator');

const clientsController = require('../controllers/clients-controller');

const router = express.Router();

router.get('/', clientsController.getAllClients);

router.get('/:cid', clientsController.getClientById);

router.post('/', [check('email').isEmail()], clientsController.addClient);

router.patch(
  '/:cid',
  [check('email').isEmail()],
  clientsController.updateClient
);

router.delete('/:cid', clientsController.deleteClient);

module.exports = router;
