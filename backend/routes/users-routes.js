const express = require('express');

const usersController = require('../controllers/users-controller');

const router = express.Router();

router.get('/', usersController.getUsers);

router.patch('/', usersController.updateUser);

router.delete('/', usersController.deleteUser);

router.post('/signup', usersController.createUser);

router.post('/login', usersController.login);

module.exports = router;
