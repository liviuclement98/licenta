const HttpError = require('../models/http-error');
const Setting = require('../models/setting');

const getSettings = async (req, res, next) => {
  let settings;
  try {
    settings = await Setting.find();
  } catch (err) {
    const error = new HttpError(
      'Something went wrong, could not get settings.',
      500
    );
    return next(error);
  }
  if (!settings || settings.length === 0) {
    const error = new HttpError(
      'Something went wrong, could not find any settings.',
      404
    );
    return next(error);
  }

  res.json({
    settings: settings.map((setting) => setting.toObject({ getters: true })),
  });
};

const addSetting = async (req, res, next) => {
  const { key, value } = req.body;
  const newSetting = new Setting({
    key,
    value,
  });

  try {
    await newSetting.save();
  } catch (err) {
    console.log(err);

    const error = new HttpError(
      'Adding a new setting failed, please try again.',
      500
    );
    return next(error);
  }

  res.status(201).json({ newSetting });
};

const updateSetting = async (req, res, next) => {
  const { key, value } = req.body;

  let setting;

  try {
    setting = await Setting.findOne({ key });
  } catch (err) {
    const error = new HttpError(
      'Something went wrong, could not update setting.',
      500
    );
    return next(error);
  }

  setting.value = value;

  try {
    await setting.save();
  } catch (err) {
    const error = new HttpError(
      'Something went wrong, could not update setting.',
      500
    );
    return next(error);
  }

  res.status(200).json({ setting: setting.toObject({ getters: true }) });
};

exports.getSettings = getSettings;
exports.addSetting = addSetting;
exports.updateSetting = updateSetting;
