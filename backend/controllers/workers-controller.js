const { validationResult } = require('express-validator');

const HttpError = require('../models/http-error');
const Worker = require('../models/worker');

const addWorker = async (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    throw new HttpError('Invalid inputs passed, please check your data.', 422);
  }

  const { name, phone, address } = req.body;
  const newWorker = new Worker({
    name,
    phone,
    address,
    dateAdded: new Date(),
  });

  try {
    await newWorker.save();
  } catch (err) {
    console.log(err);
    const error = new HttpError('Could not add worker, try again.', 500);
    return next(error);
  }

  res.status(201).json({ newWorker });
};

const getWorkers = async (req, res, next) => {
  let workers;

  try {
    workers = await Worker.find();
  } catch (err) {
    const error = new HttpError('Could not get workers, try again.', 500);
    return next(error);
  }

  if (!workers || workers.length === 0) {
    const error = new HttpError(
      'Something went wrong, could not find any workers.',
      404
    );
    return next(error);
  }

  res.json({
    workers: workers.map((worker) => worker.toObject({ getters: true })),
  });
};

const getWorkerById = async (req, res, next) => {
  const workerId = req.params.wid;

  let worker;

  try {
    worker = await Worker.findById(workerId);
  } catch (err) {
    const error = new HttpError(
      'Something went wrong, could not find worker.',
      500
    );
    return next(error);
  }

  res.json({ worker });
};

const updateWorker = async (req, res, next) => {
  const { id, name, phone, address, email, dob, sex, cnp } = req.body;

  console.log(id);
  let worker;
  try {
    worker = await Worker.findById(id);
  } catch (err) {
    const error = new HttpError(
      'Something went wrong, could not update workerr.',
      500
    );
    return next(error);
  }

  console.log('worker: ', worker);
  console.log(name, phone, address);

  worker.name = name;
  worker.phone = phone;
  worker.address = address;
  // worker.email = email;
  // worker.dob = dob;
  // worker.sex = sex;
  // worker.cnp = cnp;

  try {
    await worker.save();
  } catch (err) {
    const error = new HttpError(
      'Something went wrong, could not update worker.',
      500
    );
    return next(error);
  }

  res.status(200).json({ updatedWorker: worker.toObject({ getters: true }) });
};

const deleteWorker = async (req, res, next) => {
  const { id } = req.body;
  console.log(req.body);
  let workerToDelete;
  try {
    workerToDelete = await Worker.findById(id);
  } catch (err) {
    const error = new HttpError('Could not find worker for given id', 500);
    return next(error);
  }

  try {
    workerToDelete.remove();
  } catch (err) {
    const error = new HttpError(
      'Something went wrong, could not delete worker',
      500
    );
    return next(error);
  }

  res.status(200).json({ message: 'Worker deleted successfully!' });
};

exports.addWorker = addWorker;
exports.getWorkers = getWorkers;
exports.getWorkerById = getWorkerById;
exports.updateWorker = updateWorker;
exports.deleteWorker = deleteWorker;
