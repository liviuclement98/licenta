const { validationResult } = require('express-validator');
const HttpError = require('../models/http-error');
const User = require('../models/user');

const getUsers = async (req, res, next) => {
  let users;
  try {
    users = await User.find({}, '-password');
  } catch (err) {
    const error = new HttpError('Fetching users failed, try again later.', 500);
    return next(error);
  }
  res.json({ users: users.map((user) => user.toObject({})) });
};

const deleteUser = async (req, res, next) => {
  const { id } = req.body;
  let userToDelete;

  try {
    userToDelete = await User.findById(id);
  } catch (err) {
    const error = new HttpError('Could not find user for given id', 500);
    return next(error);
  }

  try {
    userToDelete.remove();
  } catch (err) {
    const error = new HttpError(
      'Something went wrong, could not delete client',
      500
    );
    return next(error);
  }

  res.status(200).json({ message: 'User deleted successfully!' });
};

const updateUser = async (req, res, next) => {
  const { id, username, email, role } = req.body;
  let existingUser;
  try {
    existingUser = await User.findById(id);
  } catch (err) {
    const error = new HttpError('Could not find user for given id', 500);
    return next(error);
  }

  existingUser.username = username;
  existingUser.email = email;
  existingUser.role = role;

  try {
    existingUser.save();
  } catch (err) {
    const error = new HttpError(
      'Something went wrong, could not update client',
      500
    );
    return next(error);
  }

  res.status(200).json({ updatedUser: existingUser.toObject({}) });
};

const createUser = async (req, res, next) => {
  // const errors = validationResult(req);
  // if (!errors.isEmpty()) {
  //   throw new HttpError('Invalid inputs passed, please check your data.', 422);
  // }
  const { username, email, password, role } = req.body;
  let existingUser;
  try {
    existingUser = await User.findOne({ email: email });
  } catch (err) {
    const error = new HttpError('Signing up failed, try again later.', 401);
    return next(error);
  }

  if (existingUser) {
    const error = new HttpError('User exists already.', 422);
    return next(error);
  }

  const createdUser = new User({
    username,
    email,
    role,
    password,
    dateAdded: new Date(),
  });

  try {
    await createdUser.save();
  } catch (err) {
    const error = new HttpError(
      'Creating new user failed, please try again.',
      500
    );
    return next(error);
  }

  res.status(201).json({ user: createdUser.toObject({ getters: true }) });
};

const login = async (req, res, next) => {
  const { email, password } = req.body;

  console.log(email, password);
  let existingUser;
  try {
    existingUser = await User.findOne({ email });
  } catch (err) {
    const error = new HttpError('Loggin failed, please try again later', 500);
    return next(error);
  }

  console.log(existingUser);
  console.log(password);

  if (!existingUser || existingUser.password !== password) {
    const error = new HttpError(
      'Invalid credentials, could not log you in.',
      401
    );
    return next(error);
  }
  res.json({ message: 'Logged in!' });
};

exports.getUsers = getUsers;
exports.login = login;
exports.createUser = createUser;
exports.updateUser = updateUser;
exports.deleteUser = deleteUser;
