const { validationResult } = require('express-validator');

const HttpError = require('../models/http-error');
const Client = require('../models/client');
const Appointment = require('../models/appointment');

const getClientById = async (req, res, next) => {
  const clientId = req.params.cid;
  let client;
  try {
    client = await Client.findById(clientId);
  } catch (err) {
    const error = new HttpError(
      'Something went wrong, could not find client.',
      500
    );
    return next(error);
  }
  if (!client) {
    return next(
      new HttpError('Could not find a client for the provided client id.', 404)
    );
  }

  let appointments;

  try {
    appointments = await Appointment.find({ clientId });
  } catch (err) {
    next(
      new HttpError(
        'Could not find any appointments for the provided client id.',
        404
      )
    );
  }

  console.log(appointments);

  res.json({
    client: client.toObject({ getters: true }),
    appointments: appointments,
  });
};

const getAllClients = async (req, res, next) => {
  let clients;
  try {
    clients = await Client.find();
  } catch (err) {
    const error = new HttpError(
      'Something went wrong, could not get clientss.',
      500
    );
    return next(error);
  }

  // if (!clients || clients.length === 0) {
  //   const error = new HttpError(
  //     'Something went wrong, could not find any clients.',
  //     404
  //   );
  //   return next(error);
  // }

  res.json({
    clients: clients.map((client) => client.toObject({ getters: true })),
  });
};

const addClient = async (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return next(
      HttpError('Invalid inputs passed, please check your data.', 422)
    );
  }

  const { company, name, email, phone, address } = req.body;
  const newClient = new Client({
    company,
    name,
    email,
    phone,
    address,
    addedBy: 1,
  });

  try {
    await newClient.save();
  } catch (err) {
    const error = new HttpError(
      'Adding a new client failed, please try again.',
      500
    );
    return next(error);
  }

  res.status(201).json({ client: newClient });
};

const updateClient = async (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return next(
      new HttpError('Invalid inputs passed, please check your data.', 422)
    );
  }

  const id = req.params.cid;
  const { name, email, phone, address } = req.body;

  let client;
  console.log('id: ' + id);
  try {
    client = await Client.findById(id);
  } catch (err) {
    const error = new HttpError(
      'Something went wrong, could not update clientt.',
      500
    );
    return next(error);
  }

  client.name = name;
  client.email = email;
  client.phone = phone;
  client.address = address;

  try {
    await client.save();
  } catch (err) {
    const error = new HttpError(
      'Something went wrong, could not update client.',
      500
    );
    return next(error);
  }

  res.status(200).json({ cient: client.toObject({ getters: true }) });
};

const deleteClient = async (req, res, next) => {
  const id = req.params.cid;
  let client;
  try {
    client = await Client.findById(id);
  } catch (err) {
    const error = new HttpError(
      'Something went wrong, could not delete place.',
      500
    );
    return next(error);
  }

  try {
    await Appointment.deleteMany({ clientId: client._id });
  } catch (err) {
    const error = new HttpError(
      'Something went wrong, could not delete appointments.',
      500
    );
    return next(error);
  }

  try {
    await client.remove();
  } catch (err) {
    const error = new HttpError(
      'Something went wrong, could not delete place.',
      500
    );
    return next(error);
  }

  res.status(200).json({ message: 'Client deleted successfully' });
};

exports.getClientById = getClientById;
exports.getAllClients = getAllClients;
exports.addClient = addClient;
exports.updateClient = updateClient;
exports.deleteClient = deleteClient;
