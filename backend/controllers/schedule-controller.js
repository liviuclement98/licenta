const HttpError = require('../models/http-error');

const Appointment = require('../models/appointment');
const Client = require('../models/client');
const Setting = require('../models/setting');
const Worker = require('../models/worker');

Date.prototype.addDays = function (days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
};

const getSchedule = async (req, res, next) => {
  let { from, to } = req.query;

  console.log('first: ', from, to);

  try {
    calendarColor = await Setting.findOne({
      key: 'calendar_color',
    });
    calendarColor = calendarColor.value;
  } catch (err) {
    console.log(err);
    return next(new HttpError('Could not fetch calendar color.', 500));
  }

  from = from ? new Date(from) : '';
  to = to ? new Date(to) : '';
  let appointments;

  try {
    appointments = await Appointment.aggregate([
      {
        $lookup: {
          from: 'clients',
          localField: 'clientId',
          foreignField: '_id',
          as: 'client',
        },
      },
      {
        $match: {
          datesEmployees: { $elemMatch: { date: { $gte: from, $lte: to } } },
        },
      },
    ]);
  } catch (err) {
    return next(new HttpError('Could not fetch appointments.', 500));
  }

  let workers;

  try {
    workers = await Worker.find();
  } catch (err) {
    return next(new HttpError('Could not fetch workers.', 500));
  }

  res.json({
    appointments,
    workers: workers.map((worker) => worker.toObject({ getters: true })),
    calendarColor,
    from,
    to,
  });
};

const addAppointment = async (req, res, next) => {
  let {
    canvasId,
    company,
    name,
    email,
    phone,
    address,
    description,
    datesEmployees,
    type,
    status,
    comunicatedPrice,
    chargedPrice,
    observations,
  } = req.body;

  //checking if the client already exists
  let existingClient;
  try {
    existingClient = await Client.findOne({
      $or: [{ phone }, { email }],
    });
  } catch (err) {
    const error = new HttpError('An error occured, try again later', 401);
    return next(error);
  }

  let clientId;

  //adding the client first

  if (!existingClient) {
    const newClient = new Client({
      company,
      name,
      email,
      phone,
      address,
      addedBy: 1,
    });

    try {
      await newClient.save();
    } catch (err) {
      console.log(err);
      const error = new HttpError(
        'Adding a new client failed, please try againn.',
        500
      );
      return next(error);
    }
    clientId = newClient._id;
  } else {
    clientId = existingClient._id;
  }

  datesEmployees = datesEmployees.map((dateEmployee) => ({
    ...dateEmployee,
    date: new Date(dateEmployee.date.split('-').reverse().join('-')),
  }));

  const newAppointment = new Appointment({
    canvasId,
    clientId,
    description,
    datesEmployees,
    type,
    status,
    comunicatedPrice,
    chargedPrice,
    observations,
  });

  try {
    await newAppointment.save();
  } catch (err) {
    console.log(err);
    return next(new HttpError('Could not create appointment', 500));
  }
  res.json({ newAppointment });
};

const updateAppointment = async (req, res, next) => {
  let {
    _id,
    newDatesEmployees,
    newDescription,
    newType,
    newStatus,
    newComunicatedPrice,
    newChargedPrice,
    newObservations,
  } = req.body;

  const id = _id;

  let appointment;

  try {
    appointment = await Appointment.findById(id);
  } catch (err) {
    const error = new HttpError(
      'An unexpected error occured, please try again.',
      500
    );
    return next(error);
  }

  newDatesEmployees = newDatesEmployees.map((dateEmployee) => ({
    ...dateEmployee,
    date: new Date(dateEmployee.date.split('-').reverse().join('-')),
  }));

  appointment.datesEmployees = newDatesEmployees;
  appointment.description = newDescription;
  appointment.type = newType;
  appointment.status = newStatus;
  appointment.comunicatedPrice = newComunicatedPrice;
  appointment.chargedPrice = newChargedPrice;
  appointment.observations = newObservations;

  try {
    await appointment.save();
  } catch (err) {
    console.log(err);
    const error = new HttpError(
      'Something went wrong, could not update appointment.',
      500
    );
    return next(error);
  }

  res
    .status(200)
    .json({ appointment: appointment.toObject({ getters: true }) });
};

const deleteAppointment = async (req, res, next) => {
  const { _id } = req.body;

  let appointment;
  try {
    appointment = await Appointment.findById(_id);
  } catch (err) {
    const error = new HttpError(
      'Something went wrong, could not delete appointment.',
      500
    );
    return next(error);
  }

  try {
    await appointment.remove();
  } catch (err) {
    const error = new HttpError(
      'Something went wrong, could not delete appointment.',
      500
    );
    return next(error);
  }

  res.status(200).json({ message: 'Appointment deleted successfully' });
};

exports.getSchedule = getSchedule;
exports.addAppointment = addAppointment;
exports.updateAppointment = updateAppointment;
exports.deleteAppointment = deleteAppointment;
