const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const Schema = mongoose.Schema;

const clientSchema = new Schema({
  company: { type: String },
  name: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  phone: { type: String, required: true, unique: true },
  address: { type: String, required: true },
  addedBy: { type: String },
});

clientSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Client', clientSchema);
