const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

const Schema = mongoose.Schema;

const workerSchema = new Schema({
  name: { type: String, required: true },
  phone: { type: String, required: true },
  address: { type: String, required: true },
  email: { type: String },
  sex: { type: String },
  dob: { type: Date },
  cnp: { type: String },
  dateAdded: { type: Date },
});

workerSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Worker', workerSchema);
