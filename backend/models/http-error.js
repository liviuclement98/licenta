class HttpError extends Error {
  constructor(message, errorCode) {
    super(message);
    this.code = console.error();
  }
}

module.exports = HttpError;
