const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const employeeTimesSchema = new Schema({
  id: String, //to reference worker id
  name: String,
  startTime: String,
  duration: String,
});

const dateEmployeesSchema = new Schema({
  date: Date,
  employeesTimes: [employeeTimesSchema],
});

const appointmentSchema = new Schema({
  canvasId: String,
  clientId: { type: mongoose.Types.ObjectId, required: true, ref: 'Client' },
  comunicatedPrice: Number,
  chargedPrice: Number,
  type: String,
  status: String,
  description: String,
  observations: String,
  datesEmployees: [dateEmployeesSchema],
});

module.exports = mongoose.model('Appointment', appointmentSchema);
