const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const Schema = mongoose.Schema;

const settingSchema = new Schema({
  key: { type: String, unique: true, required: true },
  value: { type: String, required: true },
});

settingSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Setting', settingSchema);
